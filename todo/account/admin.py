from django.contrib import admin
from models import CustomUser, Country, City

class CustomUserAdmin(admin.ModelAdmin):
    pass


class CountryAdmin(admin.ModelAdmin):
    pass


class CityAdmin(admin.ModelAdmin):
    pass


admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(Country, CountryAdmin)
admin.site.register(City, CityAdmin)