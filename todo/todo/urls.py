from django.conf.urls import url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', 'account.views.index', name='index'),
    url(r'^login/$', 'account.views.login_view', name='login'),
    url(r'^register/$', 'account.views.register', name='register'),
    url(r'^user_dashboard/$', 'account.views.user_dashboard', name='dashboard'),
    url(r'lang/(?P<language>\w+)/', 'account.views.translate', name='trans'),
    url(r'^logout/$', 'account.views.logout_view', name='logout'),
    url(r'^user_show/$', 'account.views.user_show', name='user_show'),
    url(r'^user_edit/$', 'account.views.user_edit', name='user_edit'),
    url(r'cities/(?P<country_id>\w+)/', 'account.views.get_cities', name='cities'),
]
