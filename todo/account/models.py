from __future__ import unicode_literals
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.db import models
from django.utils.translation import ugettext_lazy as _

class Country(models.Model):
    name = models.CharField(verbose_name=_('Country'), max_length=30)
    code = models.CharField(verbose_name=_('Country code'), max_length=5)
    class Meta:
        verbose_name = _('Country')
        verbose_name_plural = _('Countries')

    def __unicode__(self):
        return self.code


class City(models.Model):
    name = models.CharField(verbose_name=_('City'), max_length=30)
    country = models.ForeignKey(Country, verbose_name=_('Country'))
    class Meta:
        verbose_name = _('City')
        verbose_name_plural = _('Cities')

    def __unicode__(self):
        return self.name

class CustomUserManager(BaseUserManager):
    def create_user(self, username, email, password=None):
        user = self.model(
            username = username,
            email = email
        )
        user.set_password(password)
        return user

    def create_superuser(self, username, email, password=None):
        superuser = self.create_user(
            username = username,
            email = email,
            password = password
        )
        superuser.is_admin = True
        superuser.save()
        return superuser

class CustomUser(AbstractBaseUser):
    username = models.CharField(verbose_name=_('username'), max_length=30, null=False, unique=True)
    email = models.CharField(verbose_name=_('email'), max_length=50, null=False, unique=True)
    is_admin = models.BooleanField(default=False)
    city = models.ForeignKey(City, verbose_name=_('City'), null=True)
    name = models.CharField(verbose_name=_('name'), max_length=30, null=True, unique=False)

    objects = CustomUserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    def get_full_name(self):
        return self.username

    def get_short_name(self):
        return self.username

    def __str__(self):
        return self.username

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.is_admin