from django.shortcuts import render
from forms import RegisterForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, logout, login
from django.shortcuts import redirect
from django.core.exceptions import SuspiciousOperation
from django.utils import translation
from django.http import HttpResponseRedirect
from models import Country, City
from django.http import HttpResponseRedirect, JsonResponse

def index(request):
    return render(request, 'index.html')

def login_view(request):
    if request.method == 'POST':
        try:
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('/user_dashboard')
            else:
                raise SuspiciousOperation
        except ValueError:
            raise SuspiciousOperation

    return render(request,'login.html')

def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/login')
    else:
        form = RegisterForm()
    return  render(request,'register.html', {'form': form})

@login_required
def user_dashboard(request):
    return render(request, 'user_dashboard.html')

def translate(request, language):
    translation.activate(language)
    request.session[translation.LANGUAGE_SESSION_KEY] = language
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def logout_view(request):
    logout(request)
    return redirect('/')


def user_show(request):
    return render(request, 'user_show.html')


def edit_user_data(user, data):

    user.name = data['name'][0]

    city = City.objects.get(id = int(data['cities'][0]))
    user.city = city

    user.save()

@login_required
def user_edit(request):
    if request.method == 'POST':
        data = dict(request.POST.iterlists())
        user = request.user
        edit_user_data(user, data)
        return render(request, 'user_show.html')
    countries = Country.objects.all()
    return render(request, 'user_edit.html', {'countries': countries})


def get_cities(request, country_id):
    cities = list(City.objects.filter(country__id = country_id).values())
    return JsonResponse({'cities': cities})

