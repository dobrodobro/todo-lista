#-*- coding: utf-8 -*-

from django import forms
from models import CustomUser
from django.utils.translation import ugettext_lazy as _
class RegisterForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput(), label=_(u"Hasło"))
    class Meta:
        model = CustomUser
        fields = ("username", "email", "password")

    def save(self, commit=True):
            user = super(RegisterForm, self).save(commit=False)
            user.set_password(self.cleaned_data['password'])
            user.save()